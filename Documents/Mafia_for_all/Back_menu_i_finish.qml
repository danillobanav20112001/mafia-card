import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Window 2.2

Rectangle {
    id: button_finish_standGame
    visible: false
    height: 60

    Button {
        id: buttonSGame2
        background: Rectangle {
            color: "white"
            anchors.fill: parent
        }
        anchors.fill: parent
        // anchors.margins: -1
        // Layout.alignment: Qt.AlignCenter
        text: "Мы закончили!"

        onClicked: {

            close()
        }
    }
}
