import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {
    id: aboutM
    anchors.fill: parent

    background: Rectangle {
        color: bgColor
    }
    header: Back_main_menu {}
    Text {
        id: text_about
        text: qsTr("Я рад, что ты сюда зашел))\nИгра создана для удобства и наслаждения\n")
        color: "white"
        anchors.margins: defMargin
        anchors.centerIn: parent
        font.bold: true
        font.pixelSize: parent.width / 30
    }
}
