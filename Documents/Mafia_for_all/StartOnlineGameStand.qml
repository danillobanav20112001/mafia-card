import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {
    id: onlineStandGame
    property int kol_users: standGame.displayValueKolGamer
    property int kol_users_ready: 1
    header: Back_main_menu {}

    Page {
        anchors.fill: parent
        header: Label {
            height: 70
            Label {
                text: qsTr("Ждем товарищей")
                font.pixelSize: 22
                font.bold: true
                padding: 10
            }
            Label {
                text: kol_users_ready + " из " + kol_users.toString()
                font.pixelSize: 22

                font.bold: true
                anchors.right: parent.right
                padding: 10
            }
        }
        ListView {
            id: listView_TEMS
            spacing: defMargin
            anchors.fill: parent //Растягиваю на всё окно
            model: listModel_TEMS

            delegate: Rectangle {
                color: "grey"
                radius: 5
                height: 100
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: defMargin * 2
                Text {
                    id: textListElement
                    text: model.text
                    color: "#FFD700"
                    font.bold: true
                    anchors.centerIn: parent
                    font.pointSize: parent.width / 15
                }
            }
            ListModel {
                id: listModel_TEMS
                Component.onCompleted: {
                    append({
                               "text": "123456789123456789"
                           })
                }
            }
        }
    }
}
