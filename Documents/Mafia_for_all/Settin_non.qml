import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {

    header: Label {
        text: qsTr("Страница пока пустая")
        font.pixelSize: Qt.application.font.pixelSize * 2
        padding: 10
    }

    Label {
        text: qsTr("МЫ СКОРО ВЕРНЕМСЯ!")
        anchors.centerIn: parent
    }
}
