import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {
    id: settin_tems
    property int tems_num: 0
    property string tems_text: ""
    //property int indx: index
    header: Label {
        text: qsTr("Темы карточек")
        font.pixelSize: ((parent.width + parent.height) / 100) * 2
        padding: 10
    }

    // Label {
    //     text: qsTr("You are on Page 2.")
    //  anchors.centerIn: parent
    // }
    ListView {
        id: listView_TEMS
        spacing: defMargin
        anchors.fill: parent //Растягиваю на всё окно
        model: listModel_TEMS

        delegate: Rectangle {
            color: "grey"
            radius: 5
            height: 120
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: defMargin * 2

            Image {
                id: card1
                source: "qrc" + image_mass[(9 * model.num_tems) + 0]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: parent.left
                anchors.leftMargin: 3
            }

            Image {
                id: card2
                source: "qrc" + image_mass[(9 * model.num_tems) + 1]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card1.right
                anchors.leftMargin: -parent.width / 64
            }

            Image {
                id: card3
                source: "qrc" + image_mass[(9 * model.num_tems) + 2]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card2.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card4
                source: "qrc" + image_mass[(9 * model.num_tems) + 3]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card3.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card5
                source: "qrc" + image_mass[(9 * model.num_tems) + 4]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card4.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card6
                source: "qrc" + image_mass[(9 * model.num_tems) + 5]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card5.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card7
                source: "qrc" + image_mass[(9 * model.num_tems) + 6]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card6.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card8
                source: "qrc" + image_mass[(9 * model.num_tems) + 7]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card7.right
                anchors.leftMargin: -parent.width / 64
            }
            Image {
                id: card9
                source: "qrc" + image_mass[(9 * model.num_tems) + 8]
                width: parent.width / 8
                height: parent.height - 3
                anchors.left: card8.right
                anchors.leftMargin: -parent.width / 30
            }
            Rectangle {
                // rounded corners for img
                anchors.fill: parent
                color: "transparent"
                border.color: parent.color // color of background
                border.width: 5
                radius: 5
            }
            Rectangle {
                anchors.fill: parent
                color: "black"
                radius: 5
                opacity: {
                    if (settings_game.tems_for_card === model.index) {

                        0.5
                    } else {
                        0.3
                    }
                }
            }
            Text {
                id: text_image
                text: model.text
                anchors.centerIn: parent
                font.pixelSize: Qt.application.font.pixelSize * 2
                color: "white"
                font.bold: true
            }
            Text {
                id: text_image_ON
                visible: {
                    console.log("true false", tems_num)
                    console.log("true false", model.text, index)
                    if (settings_game.tems_for_card === model.index) {

                        true
                    } else {
                        false
                    }
                }
                text: "Выбрано"
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: defMargin
                font.pixelSize: Qt.application.font.pixelSize
                color: "white"
                font.bold: true
            }
            MouseArea {
                anchors.fill: parent
                onClicked: {
                    tems_num = index
                    tems_text = model.text
                    //popup_tems.open()
                    settings_game.tems_for_card = tems_num
                    //  active_main = true
                }
            }
        }
    }
    ListModel {
        id: listModel_TEMS
        Component.onCompleted: {
            console.log("VNIMAAAAAAAANIIIEEE", image_path_mass)
            var name_path_to_image = ""
            for (var i = 0; i < image_path_mass.length; i++) {
                console.log("\nVNIMAAAAAAAANIIIEEE", image_path_mass.length)
                name_path_to_image = image_path_mass[i].split(
                            "/")[image_path_mass[i].split("/").length - 1]
                append(createListElement(i, name_path_to_image))
            }
        }

        function createListElement(i, name_path_to_image) {
            console.log("ВЫВОД ПУТИ", image_path_mass[i] + "/tems_max.png")
            console.log("ВЫВОД названия", name_path_to_image)
            console.log("ВЫВОД Пути фото", "qrc" + image_mass[(9 * i) + 0])
            console.log("ВЫВОД всех фото", "qrc" + image_mass)
            return {
                "num_tems": i,
                "img": "qrc" + path_image + name_path_to_image + "/",
                "text": name_path_to_image
            }
        }


        /* ListElement {
            img: "qrc:/image/tems_max.png"
            text: "Кисть"
        }
        ListElement {
            img: "qrc:/image/tems_mine.png"
            text: "MINECRAFT"
        }*/
    }

    //ОКНО ВСПЛЫВАЮЩЕЕ


    /*Popup {
        id: popup_tems
        x: Math.round((parent.width - width) / 2)
        y: parent.height - height
        //anchors.centerIn: Overlay.overlay
        width: parent.width
        height: parent.height / 6
        //bottomPadding: -200
        // background: bgColor
        Text {
            id: text_tems_choise
            text: "Выбрать тему " + tems_text + "?"
        }
        enter: Transition {
            NumberAnimation {
                property: "opacity"
                from: 0.0
                to: 1.0
            }
        }
        exit: Transition {
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0.0
            }
        }
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        RoundButton {
            radius: 5
            height: popup_tems.height / 2
            id: backmen
            width: parent.width / 2
            anchors.left: parent.left
            anchors.top: text_tems_choise.bottom
            text: "Выбрать"
            onClicked: {

                settings_game.tems_for_card = tems_num
                popup_tems.close()
            }
        }
        RoundButton {
            height: popup_tems.height / 2
            id: sluchaino
            radius: 5
            width: parent.width / 2
            anchors.left: backmen.right
            anchors.top: text_tems_choise.bottom
            text: "Отмена"
            onClicked: popup_tems.close()
        }
    }*/
}
