import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {
    id: standGame
    readonly property int defWidth: width / 10
    property int displayValueKolGamer: 5
    property int minKolGamer: 5

    property string mirni: "Мирный"
    property string mafia: "Мафия"
    property string doctor: "Доктор"
    property string lubovnica: "Любовница"
    property string komissar: "Комиссар"
    property string maniac: "Маньяк"
    property string vedushii: "Ведущий"
    property variant countListEnamleCheckBox: ["2", "3", "4", "5"]
    property variant countListGamer: [mirni, mirni, mirni, vedushii, mafia]
    property int displayValueMafia: 1
    anchors.fill: parent

    background: Rectangle {
        color: bgColor
    }
    header: Back_main_menu {}
    footer: Start_stand_game {}

    ListView {
        id: listView
        spacing: defMargin
        anchors.fill: parent //Растягиваю на всё окно
        model: listModel

        delegate: Rectangle {

            color: "lightgrey"
            radius: 5
            height: 90
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: defMargin * 2

            Text {
                font.pixelSize: 24
                font.bold: true
                anchors.fill: parent
                anchors.margins: defMargin

                text: model.text
            }
            // Rectangle {
            Text {
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.margins: defMargin
                anchors.bottomMargin: 20
                font.bold: true
                text: model.text_more
                font.pixelSize: width / 30
                // }
            }

            Text {
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                anchors.margins: defMargin
                font.pixelSize: 28
                font.bold: true

                text: {
                    if (model.who === 'gamer') {
                        displayValueKolGamer
                    } else if (model.who === 'mafia') {
                        displayValueMafia
                    }
                }
                //model.text_num
            }
            Button {

                id: buttonMIN
                visible: model.vis_but
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                text: "-"
                font.pixelSize: 16
                font.bold: true
                width: defWidth
                anchors.margins: defMargin / 2
                anchors.leftMargin: defMargin
                anchors.rightMargin: 1

                onClicked: {

                    // for (var i = 0; i < displayValueKolGamer; i++) {

                    //  countListGamer[i] = Math.random(1, 123)
                    //  }
                    if (model.who === 'gamer'
                            && displayValueKolGamer > minKolGamer) {
                        if (displayValueKolGamer === displayValueMafia
                                || (mirniNo && displayValueMafia > 1)) {
                            displayValueMafia -= 1
                            displayValueKolGamer -= 1
                            minusMafia()
                            minusGamer()
                        } else if (countListEnamleCheckBox.length === 0) {
                            deladdNumList(listModel.get(5),
                                          false) //добавляю в лист
                            listModel.get(
                                        5).checked_box = false //отключаю галочку
                            offnCheckIfGamermax()
                            adddelCard(false, maniac) //удаляю
                            minusGamer()
                            displayValueKolGamer -= 1
                        } else {
                            minusGamer()
                            displayValueKolGamer -= 1
                        }
                    } else if (model.who === 'mafia' && displayValueMafia > 1) {
                        onnCheckIfGamermax()
                        minusMafia()
                        displayValueMafia -= 1
                    }
                }
            }
            Button {
                id: buttonPLU
                visible: model.vis_but
                anchors.bottom: parent.bottom
                anchors.left: buttonMIN.right
                text: "+"
                font.pixelSize: 16
                font.bold: true
                width: defWidth
                anchors.margins: defMargin / 2

                anchors.leftMargin: 1
                onClicked: {
                    if (model.who === 'gamer') {
                        onnCheckIfGamermax()
                        countListGamer[displayValueKolGamer] = mirni
                        standGame.displayValueKolGamer += 1
                        console.log(countListGamer)
                    } else if (model.who === 'mafia' && displayValueMafia >= 1
                               && displayValueMafia < displayValueKolGamer
                               && !mirniNo()) {

                        displayValueMafia += 1
                        plusMafia()
                        if (mirniNo()) {
                            offnCheckIfGamermax()
                        }
                    }
                }
            }
            CheckBox {
                id: checkbb
                visible: model.vis_but === false

                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.top: parent.top
                anchors.margins: defMargin
                checked: model.checked_box
                enabled: model.enableCheck
                onClicked: {
                    if (model.text === "Без Ведущего") {
                        if (model.checked_box) {
                            minusPlusVedushii(!model.checked_box)
                            model.checked_box = false
                            displayValueKolGamer++
                            minKolGamer++
                        } else {
                            minusPlusVedushii(!model.checked_box)
                            displayValueKolGamer--
                            model.checked_box = true
                            minKolGamer--
                        }
                    } else {
                        if (model.none === "none") {

                        } else {
                            adddelCard(checkState === Qt.Checked, model.text)
                            if (!mirniNo()) {
                                onnCheckIfGamermax()
                                deladdNumList(model, checkState === Qt.Checked)
                            } else {
                                deladdNumList(model, checkState === Qt.Checked)
                                offnCheckIfGamermax()
                            }
                        }
                    }

                    console.log(model.text)
                }
            }
        }
    }
    function deladdNumList(model, deladd) {

        console.log(deladd, "-Удалю или Добавлю")

        if (deladd) {
            for (var i = 0; i < countListEnamleCheckBox.length; i++) {
                if (countListEnamleCheckBox[i] === model.list_num) {
                    countListEnamleCheckBox.splice(i, 1)
                    console.log(countListEnamleCheckBox)
                }
            }
        } else {

            if (yes_no_addnumList(model)) {
                countListEnamleCheckBox.unshift(model.list_num)
                console.log(countListEnamleCheckBox)
            }
        }
    }
    function yes_no_addnumList(model) {
        var yes_no_add = true
        for (var i = 0; i < countListEnamleCheckBox.length; i++) {
            if (countListEnamleCheckBox[i] === model.list_num) {
                yes_no_add = false
                console.log("yes_no_add - falseeee")
            }
        }
        console.log("yes_no_addnumList-", countListEnamleCheckBox,
                    "model list-", model.list_num)
        return yes_no_add
    }
    function onnCheckIfGamermax() {
        console.log('<<<-------onnCheckIfGamermax--------->>>',
                    countListEnamleCheckBox)
        for (var i = 0; i < listModel.count; i++) {
            listModel.get(i).enableCheck = true
        }
    }
    function offnCheckIfGamermax() {
        console.log('<<<-------offnCheckIfGamermax--------->>>',
                    countListEnamleCheckBox)
        for (var i = 0; i < countListEnamleCheckBox.length; i++) {
            listModel.get(countListEnamleCheckBox[i]).enableCheck = false
        }
    }
    function minusPlusVedushii(truefalse) {
        console.log("=========================>", truefalse)
        if (truefalse) {
            for (var i = 0; i < countListGamer.length; i++) {
                if (countListGamer[i] === vedushii) {
                    countListGamer.splice(i, 1)

                    console.log(countListGamer)
                    break
                }
            }
        } else {

            countListGamer.unshift(vedushii)
            console.log(countListGamer)
        }
    }
    function minusGamer() {
        for (var i = 0; i < countListGamer.length; i++) {
            if (countListGamer[i] === mirni) {
                countListGamer.splice(i, 1)

                console.log(countListGamer)
                break
            }
        }
    }
    function mirniNo() {
        var yesMirn = true
        for (var i = 0; i < countListGamer.length; i++) {
            if (countListGamer[i] === mirni) {
                console.log(countListGamer)
                yesMirn = false
                break
            }
        }
        return yesMirn
    }

    function plusMafia() {

        for (var i = 0; i < countListGamer.length; i++) {
            if (countListGamer[i] === mirni) {
                countListGamer.splice(i, 1, mafia)

                console.log(countListGamer)
                break
            }
        }
    }
    function minusMafia() {

        for (var i = 0; i < countListGamer.length; i++) {
            if (countListGamer[i] === mafia) {
                countListGamer.splice(i, 1, mirni)

                console.log(countListGamer)
                break
            }
        }
    }
    function yes_no_addnumListCard(gameCard) {
        var yes_no_add = true
        for (var i = 0; i < countListGamer.length; i++) {
            if (countListGamer[i] === gameCard) {
                yes_no_add = false
                break
            }
        }
        return yes_no_add
    }
    function adddelCard(minplu, gameCard) {
        console.log(minplu, gameCard)
        if (minplu && yes_no_addnumListCard(gameCard)) {

            for (var i = 0; i < countListGamer.length; i++) {
                if (countListGamer[i] === mirni) {
                    countListGamer.splice(i, 1, gameCard)

                    console.log(countListGamer)
                    break
                }
            }
        } else {
            //minKolGamer = 5
            for (var i = 0; i < countListGamer.length; i++) {
                if (countListGamer[i] === gameCard) {
                    countListGamer.splice(i, 1, mirni)

                    console.log(countListGamer)
                    break
                }
            }
        }
    }

    ListModel {
        id: listModel

        ListElement {
            who: 'gamer'
            text: "Количество игроков"
            //text_more: "Используй + и - для того, чтобы добавить или убавить количество игроков"
            vis_but: true
        }
        ListElement {
            who: 'mafia'
            text: "Количество мафий"
            //text_more: "Используй + и - для того, чтобы добавить или убавить количество мафий"
            vis_but: true
        }
        ListElement {
            list_num: "2"
            checked_box: false
            enableCheck: true
            text: "Доктор"
            text_more: "Доктор может вылечить одного игрока за ночь"
            vis_but: false
        }
        ListElement {
            list_num: "3"
            checked_box: false
            enableCheck: true
            text: "Любовница"
            text_more: "Любовница дает алиби на один день"
            vis_but: false
        }
        ListElement {
            list_num: "4"
            checked_box: false
            enableCheck: true
            text: "Комиссар"
            text_more: "Комиссар может за ночь проверить любого\nигрока и понять, мафия тот или нет"
            vis_but: false
        }
        ListElement {
            list_num: "5"
            checked_box: false
            enableCheck: true
            text: "Маньяк"
            text_more: "Маньяк просто убивает любого за ночь"
            vis_but: false
        }
        ListElement {
            list_num: "6"
            checked_box: false
            enableCheck: true
            text: "Без Ведущего"
            text_more: "Нажмите, если сами выбрали ведущего"
            vis_but: false
            none: "none" //Сделал чтобы карты такой не было :)
            // text_more: "ПОКА НЕ РАБОТАЕТ"
        }
    }
}
