import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Window 2.2

Page {
    id: startGameStand
    visible: false

    //1 header: Back_main_menu {}
    footer: Back_menu_i_finish {
        id: button_finish_standGame
    }
    property int curIndexWitouthZero: 3
    property int curIndex: 0
    property int numCard: 1
    property bool viewSwipe_visible: false

    SwipeView {
        id: viewSwipe
        width: parent.width
        height: parent.height * 0.97
        currentIndex: 0
        // property int num_card: viewSwipe.currentIndex + 1
        property variant content: ['', '']
        // Component.onObjectNameChanged: {

        //}
        Component.onCompleted: {
            //viewSwipe_visible=true
            curIndexWitouthZero = viewSwipe.currentIndex
            curIndexWitouthZero += 1
            console.log(standGame.countListGamer.length)
            for (var i = 0; i < standGame.countListGamer.length; i++) {
                viewSwipe.addPage(viewSwipe.createPage())
            }
        }

        onCurrentIndexChanged: {

            text: standGame.countListGamer[currentIndex]

            curIndexWitouthZero = viewSwipe.currentIndex
            curIndexWitouthZero += 1
            console.log(currentIndex)
            //if (curIndex < currentIndex) {
                //viewSwipe.destroy(curIndex - 1)
               // console.log("Я УДАЛИЛ=)")
           // }
            curIndex = currentIndex
        }

        function addPage(page) {
            console.log("funzione addPage()")
            addItem(page)
            page.visible = true
        }
        function createPage() {
            var component = Qt.createComponent("RecipesPhase.qml")
            content[1] = standGame.countListGamer[numCard - 1]
            var page = component.createObject(viewSwipe, {
                                                  "text_num_Card": numCard,
                                                  "content": content,
                                                  "color": Qt.rgba(
                                                               Math.random(),
                                                               Math.random(),
                                                               Math.random(),
                                                               Math.random())
                                              })
            numCard++
            return page
        }
    }
    PageIndicator {
        id: indicator
        interactive: true
        count: viewSwipe.count
        currentIndex: viewSwipe.currentIndex
        onCurrentIndexChanged: viewSwipe.currentIndex = currentIndex

        anchors.top: viewSwipe.bottom
        anchors.topMargin: parent.height * 0.005
        anchors.horizontalCenter: parent.horizontalCenter
    }
}
