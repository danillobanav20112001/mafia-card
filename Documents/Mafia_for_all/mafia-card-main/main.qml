import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Window 2.2
import Qt.labs.settings 1.1

//import "QtAdMobActivity.java" as qtproject
ApplicationWindow {

    width: 480
    height: 720
    visible: true
    title: "Мафия"
    readonly property int defMargin: 10
    readonly property int defRadius: 5
    readonly property int defHeight: height / 14
    readonly property int defPoint: height / 32
    readonly property color panelColor: "#17212B"
    readonly property color bubbleColor: "#2b5278"
    readonly property color bgColor: "#0E1621"
    readonly property color textColor: "white"
    //property bool active_main: true
    // property int num_tems: 0

    // source: ":/qq/1.png"
    StackView {
        id: stack
        initialItem: mainmenu
        anchors.fill: parent
    }
    onClosing: {
        if (stack.depth > 1) {
            close.accepted = false
            if (startGameStand.viewSwipe_visible === true) {
                popup.open()
                //settin_tems.popup_tems.close()
            } else {
                console.log()
                stack.pop()
                // settin_tems.popup_tems.close()
            }
        } else {
            return
        }
    }
    Settings {
        id: settings_game
        //property string menu1: "Стандартная игра"
        property int tems_for_card: 0
        property variant image_card: [["qrc:/image/backCard_1.jpg", "qrc:/image/mir_1.jpg", "qrc:/image/doctor_1.jpg", "qrc:/image/kommisar_1.jpg", "qrc:/image/lubov_1.jpg", "qrc:/image/mafia_1.jpg", "qrc:/image/maniak_1.jpg", "qrc:/image/vedush_1.png", "qrc:/image/bg_1.png"], ["qrc:/image/backCard_2.png", "qrc:/image/mir_2.png", "qrc:/image/doctor_2.png", "qrc:/image/kommisar_2.png", "qrc:/image/lubov_2.png", "qrc:/image/mafia_2.png", "qrc:/image/maniak_2.png", "qrc:/image/vedush_2.png", "qrc:/image/bg_2.png"], []]
    }
    Popup {
        id: popup
        x: Math.round((parent.width - width) / 2)
        y: parent.height - height
        //anchors.centerIn: Overlay.overlay
        width: parent.width
        height: parent.height / 6
        //bottomPadding: -200
        // background: bgColor
        Text {
            id: text_close_game
            text: qsTr("Закончить Игру?")
        }
        enter: Transition {
            NumberAnimation {
                property: "opacity"
                from: 0.0
                to: 1.0
            }
        }
        exit: Transition {
            NumberAnimation {
                property: "opacity"
                from: 1.0
                to: 0.0
            }
        }
        modal: true
        focus: true
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
        RoundButton {
            radius: defRadius
            height: popup.height / 2
            id: backmen
            width: parent.width / 2
            anchors.left: parent.left
            anchors.top: text_close_game.bottom
            text: "Закончить"
            onClicked: {

                stack.pop()
                popup.close()
                startGameStand.viewSwipe_visible = false
            }
        }
        RoundButton {
            height: popup.height / 2
            id: sluchaino
            radius: defRadius
            width: parent.width / 2
            anchors.left: backmen.right
            anchors.top: text_close_game.bottom
            text: "Остаться"

            onClicked: popup.close()
        }
    }

    Page {
        id: mainmenu

        Rectangle {
            anchors.fill: parent
            color: bgColor
            Loader {
                //active: active_main
                anchors.fill: parent
                Image {
                    id: image_bg
                    anchors.fill: parent
                    source: settings_game.image_card[settings_game.tems_for_card][8]
                }
                onStatusChanged: if (loader.status === Loader.Ready)
                                     console.log("Ну как бы прошел")
            }
            Text {
                text: "Мафия"
                anchors.margins: defMargin
                anchors.top: parent.top
                anchors.left: parent.left
                color: "white"
                font.weight: parent.width * 2
                font.pointSize: parent.width / 9
            }
            RoundButton {
                radius: defRadius
                id: standG
                anchors.bottom: why_play.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: defMargin / 6
                anchors.rightMargin: defMargin * 2.5
                anchors.leftMargin: defMargin * 2.5
                height: defHeight
                text: "Стандартная игра"
                font.pointSize: defPoint
                onClicked: stack.push(standGame)
            }
            RoundButton {
                radius: defRadius
                id: why_play
                anchors.bottom: setting.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: defMargin / 6
                anchors.rightMargin: defMargin * 2.5
                anchors.leftMargin: defMargin * 2.5
                height: defHeight
                text: "Как играть?"
                font.pointSize: defPoint
                onClicked: stack.push(why_play_page)
            }
            RoundButton {
                radius: defRadius
                id: setting
                anchors.bottom: about.top
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: defMargin / 6
                anchors.rightMargin: defMargin * 2.5
                anchors.leftMargin: defMargin * 2.5
                height: defHeight
                text: "Настройки"
                font.pointSize: defPoint
                onClicked: stack.push(settin)
            }
            RoundButton {
                radius: defRadius
                id: about
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.margins: defMargin / 6
                anchors.rightMargin: defMargin * 2.5
                anchors.leftMargin: defMargin * 2.5
                height: defHeight

                text: "О нас"
                font.pointSize: defPoint
                onClicked: stack.push(aboutM)
            }
        }
    }
    MyStandGame {
        id: standGame
        visible: false
    }

    AboutM {
        id: aboutM
        visible: false
    }
    Settin {
        id: settin
        visible: false
    }
    Why_play {
        id: why_play_page
        visible: false
    }

    StartGameStand {
        id: startGameStand
    }
}
