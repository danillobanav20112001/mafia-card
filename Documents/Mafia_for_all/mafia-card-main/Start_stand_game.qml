import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Rectangle {
    id: button_start_standGame

    height: 60

    Button {
        id: buttonSGame
        background: Rectangle {
            color: "white"
            anchors.fill: parent
        }
        anchors.fill: parent
        // anchors.margins: -1
        // Layout.alignment: Qt.AlignCenter
        text: "НАЧАТЬ ИГРУ"

        onClicked: {
            console.log("caaaaaaaaaaaaaaaaaarsss", settings_game.tems_for_card)
            console.log(settings_game.image_card[settings_game.tems_for_card][1])
            startGameStand.viewSwipe_visible = true
            var randomstart = []
            var cikle = standGame.countListGamer.length
            for (var i = 0; i < cikle; i++) {
                var randomCount = getRandomInt(0,
                                               standGame.countListGamer.length)
                randomstart[i] = standGame.countListGamer[randomCount]
                console.log(randomCount)
                standGame.countListGamer.splice(randomCount, 1)
            }
            standGame.countListGamer = randomstart

            stack.push("StartGameStand.qml")
        }
        function getRandomInt(min, max) {
            return Math.floor(Math.random() * (max - min)) + min
        }
    }
}
