import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5
import QtQuick.Window 2.2

//import "QtAdMobActivity.java" as qtproject
Page {
    id: why_play_page
    header: Back_main_menu {}
    anchors.fill: parent
    background: Rectangle {
        color: bgColor
    }
    Flickable {

        anchors.fill: parent
        width: parent.width
        height: parent.height
        contentHeight: {
            cel_game.height + aboutmaf.height + opic_cart_and_rol.height
                    + vedushi.height + aboutVed.height + mafia.height + aboutMafi.height
                    * 2 + komiss.height + aboutkomiss.height + doctor.height
                    + aboutdoctor.height * 2 + mirnJit.height + aboutmirnJit.height
                    + maniak.height + aboutmaniak.height
                    + lubovnica.height + aboutlubovnica.height * 2
        }

        ScrollBar.vertical: ScrollBar {
            id: vbar
            active: vbar.active
        }

        Text {
            id: cel_game
            text: "Цель игры Мафия"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            font.pointSize: parent.width / 15
            anchors.margins: defMargin
        }
        Text {
            id: aboutmaf
            anchors.top: cel_game.bottom
            text: qsTr("Цель каждого играющего – сделать все, чтобы его команда победила.
К примеру, мирные граждане должны выявить всех мафиози.
Игроки из противоположной группы должны выдать себя за честных
граждан и извести всех рядовых жителей города.Любой участник имеет
право высказать свои подозрения о другом играющем.
Допускается обвинять друг друга в связях с бандитами
и выдвигать кандидатуры на голосование.
Обвиняемые вправе:
► Обвинить в связях с преступникам других игроков;
► Защитить свою кандидатуру от наговоров.
Начинается голосование. Если против человека проголосовали все
остальные члены команды или большинство, то он считается убитым,
а его карта переворачивается. Только после этого раскрывается секрет
– кем действительно был «убитый». Сюжет повторяется, пока не будут
уничтожены все члены бандитской группировки или честные жители.
Если вы не знаете, как научиться играть в мафию, то ознакомьтесь
с описанием карт и ролей.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: opic_cart_and_rol
            text: "Описание карт и ролей"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutmaf.bottom
            font.pointSize: parent.width / 15
            anchors.margins: defMargin
        }
        Text {
            id: vedushi
            text: "Ведущий"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: opic_cart_and_rol.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutVed
            anchors.top: vedushi.bottom
            text: qsTr("Назначение ведущего обеспечит мониторинг игрового процесса
и соблюдение всех правил. Он раздает карточки для мафии всем
членам по своему усмотрению или наугад, в результате чего разделяет
всю компанию на два противоборствующих лагеря. Только ему известно,
какой персонаж к какой команде относится. Он объявляет наступление
дня и ночи, выносит на голосование кандидатуры потенциальных
мафиози.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: mafia
            text: "Мафия"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutVed.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutMafi
            anchors.top: mafia.bottom
            text: qsTr("Цель всех мафиози, как сказано выше,
– уничтожить всех без исключения членов другой группы. Следует
учесть, что мирных жителей в 3-5 раз больше, чем мафиози, поэтому
последним дается шанс убийства одного из честных людей ночью.
Помимо этого члены данной команды имеют неоспоримое
преимущество: они знают всех членов своей группы в лицо. Когда
ведущий говорит: «Наступает ночь. Просыпается мафия», все
участники команды открывают глаза и решают, кого они сейчас
убьют. Играющие должны это сделать без единого звука,
исключительно жестами.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: komiss
            text: "Комиссар (или Шериф)"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutMafi.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutkomiss
            anchors.top: komiss.bottom
            text: qsTr("Шериф просыпается ночью, когда все спят. Он имеет право
показать пальцем на любого играющего, чтобы выявить мирного
человека или бандита. Если участник, в которого шериф показал
пальцем, является игровым преступником, то ведущий положительно
кивает головой. Если этот персонаж не угадал, то ведущий
кивает отрицательно. Как вы понимаете, шериф заодно с честными
гражданами, а не с бандитами. Его первоочередная задача – сделать
все, чтобы после того, как он выявил бандита, за эту кандидатуру
проголосовали мирные жители.

Никто не должен знать, кто конкретно шериф. Если члены группы
бандитской группы об этом узнают, то скорей всего, его «уберут»
сразу же. Суть роли шерифа заключается в том, чтобы убедить честных
жителей голосовать за убийство бандита, не навлекая на себя
подозрений. Если у шерифа детективный склад ума, победа будет за
мирными гражданами. Версий такого развлечения много: порой игра
для компании прекращается, когда этот персонаж умирает. Тогда
победа засчитывается мафиози.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: doctor
            text: "Доктор"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutkomiss.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutdoctor
            anchors.top: doctor.bottom
            text: qsTr("Доктор играет за мирных людей. Он может спасти как мафиози,
так и простого гражданина. Когда город просыпается, доктор должен
понять, кого бандиты захотят «убрать», чтобы вылечить этого
участника. Если же доктору это не удается сделать, то он лечит
любого человека наугад. Доктор не может лечить одного и того
же играющего две ночи подряд, но единожды способен вылечить себя.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: mirnJit
            text: "Мирные жители"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutdoctor.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutmirnJit
            anchors.top: mirnJit.bottom
            text: qsTr("Эта роль, по сути, самая нефункциональная. Честный житель наименее
осведомлен. Этот человек ночью спит, а днем должен активно
участвовать во всех обсуждениях с другими участниками. Роль
«обычного гражданина» заключается в том, чтобы выявить
мафиози, используя свою логику и интуицию. Однако под ловким
предводительством бандиты собьют мирного гражданина с толку,
и на голосовании он может проголосовать против членов своей
же команды.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: maniak
            text: "Маньяк"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutmirnJit.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutmaniak
            anchors.top: maniak.bottom
            text: qsTr("Маньяк играет сам за себя. Задача Маньяка избавиться от всех
Мирных жителей и от Мафии. Каждую ночь имеет право «убить» одного
игрока.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
        Text {
            id: lubovnica
            text: "Любовница"
            color: "white"
            font.bold: true
            anchors.left: parent.left
            anchors.top: aboutmaniak.bottom
            font.pointSize: parent.width / 23
            anchors.margins: defMargin
        }
        Text {
            id: aboutlubovnica
            anchors.top: lubovnica.bottom
            text: qsTr("Проводит ночь с одним из жителей городка, мешая ему при этом
днём ему выполнять свою спецфункцию: вести обсуждение и голосовать.
Относится к мирным жителям.")
            anchors.left: parent.left
            font.pointSize: parent.width / 40
            anchors.margins: defMargin
            wrapMode: Text.WordWrap
            color: "white"
        }
    }
}
