import QtQuick 2.0

//import QtGraphicalEffects 1.0
//Графика не работает
Rectangle {
    id: card
    property bool viewSwipe_visible: false
    property bool running_animation: false
    property string text_back: ""
    property string text_back_fist: "  Свайпни вправо\nи передай телефон\nследующему игроку"
    property string text_back_fist_card: "Ты последний"
    property string text_num_Card: ""
    property variant content: ['', '']

    Component.onCompleted: {

        // console.log("Я создан", currentIndex)
        //Дописать закрытие карты после перехода на новую карту
    }
    Item {
        id: flipme_item
        width: parent.width
        height: parent.height
        property int total_count: 0
        property int front_img: 0
        property int back_img: 1
        property bool flip_now: false

        Flipable {
            id: flip_element
            width: parent.width * 0.85
            height: parent.height * 0.8

            anchors.centerIn: parent
            front: Rectangle {
                radius: Math.min(width / 40, height / 40)
                anchors.fill: parent
                anchors.centerIn: parent
                color: bgColor
                Image {
                    width: parent.width - 6
                    height: parent.height - 6
                    anchors.centerIn: parent
                    id: photoPreview2
                    source: settings_game.image_card[settings_game.tems_for_card][0]
                }
                Rectangle {
                    // rounded corners for img
                    anchors.fill: parent
                    color: "transparent"
                    border.color: parent.color // color of background
                    border.width: 8
                    radius: Math.min(width / 40, height / 40)
                }
                Rectangle {
                    // rounded corners for img
                    visible: if (text_back) {
                                 true
                             } else {
                                 false
                             }
                    anchors.fill: parent
                    color: "black"
                    opacity: 0.5
                    radius: 5
                }
                Text {
                    id: frontText

                    text: text_back
                    font.bold: true
                    font.pixelSize: parent.width * 0.07
                    font.family: "Comic Sans MS"
                    color: "white"

                    anchors.centerIn: parent
                }

                Text {
                    id: numPage2
                    text: text_num_Card //viewSwipe.num_card

                    font.pixelSize: parent.width * 0.1
                    font.family: "Comic Sans MS"
                    font.bold: true
                    color: "white"
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: defMargin
                }
            }

            back: Rectangle {
                Image {
                    width: parent.width - 6
                    height: parent.height - 6
                    anchors.centerIn: parent
                    id: photoPreview
                    source: {
                        if (content[flipme_item.back_img] === "Мирный") {
                            settings_game.image_card[settings_game.tems_for_card][1]
                        } else if (content[flipme_item.back_img] === "Мафия") {
                            settings_game.image_card[settings_game.tems_for_card][5]
                        } else if (content[flipme_item.back_img] === "Доктор") {
                            settings_game.image_card[settings_game.tems_for_card][2]
                        } else if (content[flipme_item.back_img] === "Любовница") {
                            settings_game.image_card[settings_game.tems_for_card][4]
                        } else if (content[flipme_item.back_img] === "Комиссар") {
                            settings_game.image_card[settings_game.tems_for_card][3]
                        } else if (content[flipme_item.back_img] === "Маньяк") {
                            settings_game.image_card[settings_game.tems_for_card][6]
                        } else if (content[flipme_item.back_img] === "Ведущий") {
                            settings_game.image_card[settings_game.tems_for_card][7]
                            //settings_game.image_card[settings_game.tems_for_card][0]
                        }
                    }
                    // fillMode: Image.PreserveAspectFit
                }
                Rectangle {
                    // rounded corners for img
                    anchors.fill: parent
                    color: "transparent"
                    border.color: parent.color // color of background
                    border.width: 8
                    radius: Math.min(width / 40, height / 40)
                }
                radius: Math.min(width / 40, height / 40)
                anchors.fill: parent
                anchors.centerIn: parent
                color: bgColor
                Text {
                    id: backText
                    text: content[flipme_item.back_img]
                    font.pixelSize: parent.width * 0.15
                    font.family: "Comic Sans MS"
                    font.bold: true
                    color: "white"
                    anchors.centerIn: parent
                }
                Text {
                    id: numPage
                    text: text_num_Card //viewSwipe.num_card
                    font.pixelSize: parent.width * 0.1
                    font.family: "Comic Sans MS"
                    font.bold: true
                    color: "white"
                    anchors.top: parent.top
                    anchors.right: parent.right
                    anchors.rightMargin: defMargin
                }
            }

            transform: Rotation {
                id: rotation
                origin.x: flip_element.width / 2
                origin.y: flip_element.height / 2
                axis.x: 0
                axis.y: 1
                axis.z: 0 //set axis.x to 1 to rotate around x-axis
                angle: 0
            }


            /*Flipable element rotates from 0 to 180 degree during state change defined by "when" property.
            If the "when" property fails, it reverts back to the default state of 0 deg.*/
            states: State {
                name: "value_change"
                PropertyChanges {
                    target: rotation
                    angle: 180
                }
                when: flipme_item.flip_now
            }
            transitions: Transition {
                NumberAnimation {
                    target: rotation
                    property: "angle"
                    duration: 600
                }
            }
        }
        Timer {
            id: img_change_timer

            running: running_animation
            repeat: running_animation
            interval: 2000
            onTriggered: {
                flipme_item.total_count = flipme_item.total_count + 1
                if (flipme_item.total_count > 10)

                    flipme_item.total_count = 0
            }
        }
        onTotal_countChanged: {
            if (flipme_item.flip_now == false) {
                img_change_timer.interval = 2000
                //running_animation = false
                flipme_item.back_img = flipme_item.total_count
            } else {
                flipme_item.front_img = flipme_item.total_count
                if (button_finish_standGame.visible === false) {
                    text_back = text_back_fist
                } else {
                    if (viewSwipe.currentIndex + 1 === standGame.countListGamer.length) {
                        text_back = text_back_fist_card
                    } else {
                        text_back = text_back_fist
                    }
                }
                running_animation = false
            }
            flipme_item.flip_now = !flipme_item.flip_now
        }
    }
    MouseArea {
        anchors.fill: parent
        onClicked: {
            if (viewSwipe.currentIndex + 1 === standGame.countListGamer.length) {
                button_finish_standGame.visible = true
            } // else {
            //   button_finish_standGame.visible = false
            // }
            //console.log(delegate.SwipeView.view.incrementCurrentIndex())
            card.running_animation = true
            img_change_timer.interval = 50
        }
    }
}
