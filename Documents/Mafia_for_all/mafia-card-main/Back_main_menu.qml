import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Rectangle {
    id: button_back_menu
    color: bgColor
    height: 40

    RowLayout {
        anchors.fill: parent
        anchors.margins: defMargin
    }
    Button {
        id: buttonMmenu
        anchors.left: parent.left
        height: parent.height
        width: parent.width / 10
        anchors.margins: -1
        Layout.alignment: Qt.AlignCenter

        background: Rectangle {
            color: bgColor
        }

        Text {
            anchors.centerIn: parent
            text: "￩"
            color: "white"
            font.pointSize: 30
            font.bold: true
        }

        onClicked: {
            stack.pop()
        }
    }
}
