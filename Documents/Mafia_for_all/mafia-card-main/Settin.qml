import QtQuick 2.15
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.5

Page {

    id: settin
    anchors.fill: parent

    background: Rectangle {
        color: bgColor
    }
    header: Back_main_menu {}
   // Text {
     //   id: text_about
      //  text: qsTr("")
      //  color: "white"
      //  anchors.margins: defMargin
      //  anchors.centerIn: parent
      //  font.bold: true
      //  font.pixelSize: parent.width / 30
   // }
    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        Settin_tems {
        }

        Settin_non {
        }
    }

    footer: TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex

        TabButton {
            text: qsTr("Темы")
        }
        TabButton {
            text: qsTr("NON")
        }
    }
}
