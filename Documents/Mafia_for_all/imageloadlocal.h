#ifndef IMAGELOADLOCAL_H
#define IMAGELOADLOCAL_H

#include <QObject>
#include <QList>
#include <QQmlApplicationEngine>
#include <QQmlContext>

class ImageLoadLocal : public QObject
{
    Q_OBJECT
     QQmlApplicationEngine engine;
     QList<QString> NameTems;
public:
    explicit ImageLoadLocal(QObject *parent = nullptr);

signals:
    void uploadedImage();
};

#endif // IMAGELOADLOCAL_H
